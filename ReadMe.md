<!--
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH (hereon)
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de
-->

This is a collection of dockerfiles to test the build of MOSSCO on various
platforms.

The dockerfiles are contained in the subdirectory `./dockerfiles`.  With a
local Docker daemon running, you can build the containers by issuing

```
docker build . -t <image-name> -f dockerfiles/<docker-file>
```

Please substitute `<image-name>` with a name of your choice and `<docker-file>`
with the filename in `dockerfiles` you'd like to test. You may need to run
with elevated privileges (`sudo`).

# List of dockerfiles

- `dockerfiles/Dockerfile.ubuntu-impish.gnu-mpich`: A linux ubuntu/impish
  (21.10) image with gnu/mpich toolchain

# Executing the container

Mount the local work directory to the container's work directory `~/work` and
create a container, named with the `--name` argument.  For a local work
directory `~/devel/mossco/mossco-setups/sns` and an image
named `mossco-ubuntu`, the command should be:

```
docker run -v ~/~/devel/mossco/mossco-setups/sns:~/work
--name=mossco -t mossco-ubuntu /bin/bash
```
You can also provide the mossco executable as the last argument to the `docker
run` command, or exit it interactively in the container.

```
docker run  -v ~/devel/mossco/mossco-setups/sns:/home/mossco/work --name=mossco -t mossco-ubuntu-20 /home/mossco/bin/gfs
```

## Interacting with the containers
```
docker run -it mossco-ubuntu /bin/bash
```
