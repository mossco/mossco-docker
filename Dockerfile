# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum Hereon
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor: Carsten Lemmen <carsten.lemmen@hereon.de

ARG COMMUNICATOR="openmpi"
ARG FABM="mossco"
FROM registry.hzdr.de/schism/esmf-docker/jammy-gnu:v8.4.0-${COMMUNICATOR}

LABEL description="MOSSCO-ESMF Docker environment based on Ubuntu"
LABEL author="Carsten Lemmen <carsten.lemmen@hereon.de>"
LABEL license="CC0-1.0"
LABEL copyright="2023 Helmholtz-Zentrum Hereon"

# Further arguments can be passed via the --build-arg key=value command to the 
# docker build command.  The default values are set below to superbee 
ARG COMMUNICATOR="openmpi"
ARG FABM="mossco"

# Make ARG variables available within the container
#ENV TVD_LIM ${TVD_LIM}
#ENV COMMUNICATOR openmpi
ENV COMMUNICATOR ${COMMUNICATOR}
ENV FABM ${FABM}

RUN apt-get update && apt-get -qy upgrade
RUN apt-get update && apt-get -qy \
    install make lib${COMMUNICATOR}-dev \
    cmake wget python3 python3-pip \
    python-is-python3 libnetcdf-dev \
    libnetcdff-dev libxerces-c-dev liblapack-dev libyaml-cpp-dev \
    subversion cvs git \
    gcc-11 gfortran-11 g++-11

# Remove all mpich related packages if communicator is not mpich, or
# do the same for openmpi
RUN if [ "x${COMMUNICATOR}" != "xmpich" ] ; then apt-get remove -qy *mpich* ; fi
RUN if [ "x${COMMUNICATOR}" != "xopenmpi" ] ; then apt-get remove -qy *openmpi* ; fi

ENV PATH="/usr/lib64/${COMMUNICATOR}/bin:${PATH}:/opt/bin"
ENV MOSSCO_INSTALL_PREFIX=/opt
ENV MOSSCO_DIR="/usr/src/mossco/code"
ENV MOSSCO_SETUPDIR="/usr/src/mossco/setups"

WORKDIR /usr/src
SHELL ["/bin/bash", "-c"]

RUN git clone  --branch master --depth 1 https://codebase.helmholtz.cloud/mossco/code.git ${MOSSCO_DIR}
RUN make -C ${MOSSCO_DIR} external
RUN mkdir -p /usr/src/fabm

RUN if [[ "x${FABM}" == "gcoast" ]] ; then \
  export MOSSCO_FABMDIR="/usr/src/fabm/gcoast" ; \
  export FABM_GIT="https://codebase.helmholtz.cloud/GCOAST/gcoast-fabm.git" ; \ 
  git clone  --branch master --depth 1 ${FABM_GIT} ${MOSSCO_FABMDIR}; \
  fi
RUN if [[ "x${FABM}" == "official" ]] ; then \
  export MOSSCO_FABMDIR="/usr/src/fabm/official" ; \
  export FABM_GIT="https://github.com/fabm-model/fabm.git" ; \
  git clone  --branch master --depth 1 ${FABM_GIT} ${MOSSCO_FABMDIR}; \
  fi
RUN if [[ "x${FABM}" == "schism" ]] ; then \
  export MOSSCO_FABMDIR="/usr/src/fabm/official" ; \
  export FABM_GIT="https://github.com/schism-dev/fabm.git" ; \
  git clone  --branch master --depth 1 ${FABM_GIT} ${MOSSCO_FABMDIR}; \
  fi

RUN make -C ${MOSSCO_DIR} libjson_external 
RUN make -C ${MOSSCO_DIR} libfabm_external 
RUN make -C ${MOSSCO_DIR} libgotm_external 
RUN make -C ${MOSSCO_DIR} libgetm_external

RUN make -C ${MOSSCO_DIR}/src/utilities all
RUN make -C ${MOSSCO_DIR}/src/drivers all
RUN make -C ${MOSSCO_DIR}/src/components all
RUN make -C ${MOSSCO_DIR}/src/mediators all
RUN make -C ${MOSSCO_DIR} all

RUN make -C ${MOSSCO_DIR} install
