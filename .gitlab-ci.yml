# GitLab CI configuration file for building SCHISM in an ESMF
# docker image based on ubuntu/gfortran/mpich
# 
# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileContributor Carsten Lemmen <carsten.lemmen@hereon.de

stages:
    - lint
    - build
    - deploy
    - test

variables:
    VERSION: v8.4.0
    CONTAINER_REGISTRY_IMAGE: $CI_REGISTRY_IMAGE/mossco:esmf${VERSION}-gfortran-${COMMUNICATOR}-fabm-${FABM}

.base:
    image: docker
    tags: ["docker", "dind"]
    services:
        - docker:dind
    parallel:
        matrix: 
            - COMMUNICATOR: ["openmpi"]
              FABM: ["mossco"]
    allow_failure: false
    interruptible: true

build-one:
    extends: .base
    stage: build
    before_script: 
        - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    script:
        - docker build -t ${CONTAINER_REGISTRY_IMAGE} --build-arg VERSION=${VERSION} --build-arg COMMUNICATOR=${COMMUNICATOR} .
        - docker push ${CONTAINER_REGISTRY_IMAGE}

build-all:
    extends: .base
    stage: deploy
    parallel:
        matrix: 
            - COMMUNICATOR: ["openmpi", "mpich"]
              FABM: ["mossco", "official", "gcoast", "schism"]
    before_script: 
        - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    script:
        - docker build -t ${CONTAINER_REGISTRY_IMAGE} \
           --build-arg VERSION=${VERSION} \
           --build-arg COMMUNICATOR=${COMMUNICATOR} 
           --build-arg FABM=${FABM} .
        - docker push ${CONTAINER_REGISTRY_IMAGE}
    needs:
        - build-one
    allow_failure: true

license-compliance:
    image: python:3.11
    stage: lint
    before_script:
        - pip install --upgrade pip
        - pip install poetry
        - poetry install
    script:
        - poetry run reuse lint
    allow_failure: true

test-one:
    extends: .base
    stage: test
    before_script: 
        - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    script:
        - docker pull ${CONTAINER_REGISTRY_IMAGE}
        - docker run ${CONTAINER_REGISTRY_IMAGE} /opt/bin/mossco -h
    needs: 
        - build-one

test-all:
    extends: .base
    stage: test
    parallel:
        matrix: 
            - COMMUNICATOR: ["openmpi", "mpich"]
              FABM: ["mossco", "official", "gcoast", "schism"]
    before_script: 
        - docker login -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN $CI_REGISTRY

    script:
        - docker pull ${CONTAINER_REGISTRY_IMAGE}
        - docker run ${CONTAINER_REGISTRY_IMAGE} /opt/bin/mossco -h
    needs: 
        - build-all
    allow_failure: true